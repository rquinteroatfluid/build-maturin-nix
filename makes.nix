{fetchNixpkgs, ...}: {
  extendingMakesDirs = ["/"];
  inputs = {
    nixpkgs = fetchNixpkgs {
      rev = "f1010e0469db743d14519a1efd37e23f8513d714";
      sha256 = "sha256-doPgfj+7FFe9rfzWo1siAV2mVCasW+Bh8I1cToAXEE4=";
    };
  };
}
