# shellcheck shell=bash

function main {
    : \
        && python3 "__argSrc__/main.py" \
        || return 1
}

main "${@}"
