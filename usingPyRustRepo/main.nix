{
  inputs,
  fetchGitlab,
  makeScript,
  makePythonEnvironment,
  projectPath,
  ...
}: let
  nixpkgs = inputs.nixpkgs;
  python_version = "python311";
  inherit (nixpkgs."${python_version}".pkgs) buildPythonPackage;
  python_pkgs = nixpkgs."${python_version}Packages";

  basic-rust-python-integration = buildPythonPackage rec {
    pname = "basic-rust-python-integration";
    version = "822c1e7ac10e2516b3bc7ce8bc23dba7273e4e6f";

    src = fetchGitlab {
      owner = "rquinteroatfluid";
      repo = pname;
      rev = version;
      sha256 = "sha256-j6zSqMKSoiC43Es4+5HLwqQkEN0je24IEEQowemiaiI=";
    };

    cargoDeps = nixpkgs.rustPlatform.fetchCargoTarball {
      inherit src;
      hash = "sha256-wICmSifx2ESTL2bwPQW+0iHpQiCP+HcB1rgv5wf9cnU=";
    };

    format = "pyproject";

    nativeBuildInputs = with nixpkgs.rustPlatform; [cargoSetupHook maturinBuildHook];
  };

  pythonEnvironment = makePythonEnvironment {
    pythonProjectDir = projectPath "/usingPyRustRepo/src";
    pythonVersion = "3.11";
  };
in
  makeScript {
    name = "using-rust-from-python";
    entrypoint = ./entrypoint.sh;

    replace = {
      __argSrc__ = projectPath "/usingPyRustRepo/src";
    };

    searchPaths = {
      bin = [
        nixpkgs."${python_version}"
        # env
      ];
      source = [pythonEnvironment];
      pythonPackage = [
        "${basic-rust-python-integration}/lib/python3.11/site-packages/"
      ];
    };
  }
