from basic_rust_python_integration import (
    sum_as_string,
)

def main():
    assert sum_as_string(1, 2) == "3"
    print(sum_as_string(1, 2))

if __name__ == "__main__":
    main()
